const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')


let path = "/dev/ttyA102";

const port = new SerialPort(path, {
   baudRate: 9600,
   isOpen: true,
   lock: true,
   endOnClose: true
});

const parser = new Readline();
port.pipe(parser);


port.on('data', (data)=>{
	console.log('Received:', data.toString());
        
//	let dataStr = data.toString();
//	if(dataStr === "H\r") {
//		console.log('HB <ACK>');
//		port.write("\x06");
//	} else {
//		let trama = `\u0002201TIMESTAMP:2020-09-25T21:17:25.886Z;ANI:3132830998;PRECISION:55  ;NOMBRE:Camilo Jimémenz                                                                           ;DIRECCION:KR 100 #10-10                                                                             ;LAT:4.546796   ;LONG:-75.0123   ;BARRIO:SANTA MARIA                                                                               ;LOCALIDAD:BARRIOS UNIDOS                                                                            \u0003\r`; 
//		
//		console.log('POS <- VESTA', trama);
//
//		port.write(trama);
//	}

});



parser.on('data', line => console.log(`> ${line}`))

port.on('error', function(err) {
  console.log('Error: ', err.message)
});

console.log(' ');
console.log(`Puerto ${path} iniciado.`);


//setInterval( () => {
	//console.log('* sin actividad ' + Date.now());
//}, 10000);
